package com.jjt.hello.spring.cloud.alibaba.nacos.consumer.feign.service;

import com.jjt.hello.spring.cloud.alibaba.nacos.consumer.feign.fallback.HelloServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "nacos-provider",fallback = HelloServiceFallback.class)
public interface HelloService {
    @GetMapping("/h/{msg}")
    public String hello(@PathVariable("msg") String msg);
}
