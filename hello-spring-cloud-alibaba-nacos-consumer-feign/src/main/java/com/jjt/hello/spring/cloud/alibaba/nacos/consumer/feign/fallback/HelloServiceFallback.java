package com.jjt.hello.spring.cloud.alibaba.nacos.consumer.feign.fallback;

import com.jjt.hello.spring.cloud.alibaba.nacos.consumer.feign.service.HelloService;
import org.springframework.stereotype.Component;

@Component
public class HelloServiceFallback implements HelloService {

    @Override
    public String hello(String msg) {
        return "请检查网络连接是否错误";
    }
}
