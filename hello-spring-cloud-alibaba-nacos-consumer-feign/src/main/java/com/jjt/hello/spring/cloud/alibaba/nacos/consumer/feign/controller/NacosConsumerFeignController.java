package com.jjt.hello.spring.cloud.alibaba.nacos.consumer.feign.controller;

import com.jjt.hello.spring.cloud.alibaba.nacos.consumer.feign.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NacosConsumerFeignController {
    @Autowired
    private HelloService helloService;
    //http://127.0.0.1:9092/c/hi
    @GetMapping("/c/hi")
    public String hell(){
       return helloService.hello(":我是feign消费者调用的");

    }
}
